import pylab as pl
from scipy.io import loadmat
from numpy import *
from matplotlib import rc
rc('font',**{'family':'sans-serif','sans-serif':['Helvetica']})
pl.rcParams['axes.grid'] = True

fname ='EMB217.mat'
fname = 'EMB217_small.mat'
STA = loadmat(fname)['STA']
CTD = loadmat(fname)['CTD']
MIX = loadmat(fname)['MIX']

nprofiles = shape(STA)[1]
print(STA.dtype)
print(CTD.dtype)
for i in range(nprofiles):
    print(i)
    lon = STA['LON'][0,i][0][0]
    lat = STA['LAT'][0,i][0][0]
    date = STA['date'][0,i][0]
    print(lon,lat,date)
    STA['LON'][0,i]
    PCTD = CTD['P'][0,i]
    TCTD = CTD['T'][0,i]
    SCTD = CTD['S'][0,i]
    O2CTD = CTD['O2'][0,i]
    PMIX = MIX['P'][0,i]
    EPSMIX = MIX['eps'][0,i]        
    pl.figure(1)
    pl.clf()
    pl.grid()
    pl.subplot(1,4,1)#.tick_params (axis = 'y', which = 'both', labelleft = 'off')
    pl.xlabel ('$T\ /\ \degree \mathrm{C}$')
    pl.ylabel ('Depth / m')
    pl.plot(TCTD,-PCTD)
    pl.title(date)
    pl.subplot(1,4,2).tick_params (axis = 'y', which = 'both', labelleft = 'off')
    pl.xlabel ('$S\ /\ \mathrm{g}\ \mathrm{kg}^{-1}$')
    pl.plot(SCTD,-PCTD)
    pl.subplot(1,4,3).tick_params (axis = 'y', which = 'both', labelleft = 'off')
    pl.xlabel ('$O_2\ /\ \mu\mathrm{mol}\ \mathrm{l}^{-1}$')
    pl.plot(O2CTD,-PCTD)
    pl.subplot(1,4,4).yaxis.tick_right()
    pl.xlabel ('$\log\ \epsilon\ /\ \mathrm{W}\ \mathrm{kg}^{-2}$')
    pl.plot(log10(EPSMIX),-PMIX)        
    pl.draw()
    pl.show()    
#    input('fdsfsfd')

