## setze gnuplot grafiken

FIG := $(wildcard *.gp)
MAIN := posterWTD19.tex
BIBMAIN := $(patsubst %.tex,%,$(MAIN))

ALLFIGS:= $(patsubst %.gp,%,$(OUTFIG))

print:
	@echo $(FIG)

GP := gnuplot -e " set terminal epslatex color"

figs:
	$(GP) $(FIG)
	
	@echo ""
	@echo "THE FIGURES $(FIG) GOT GENERATED!"
	@echo ""

BIB := biber $(BIBMAIN)

bib:
	$(BIB)
	@echo ""
	@echo "THE BIB FILES $(MAIN) GOT GENERATED!"
	@echo ""

LATEX := pdflatex $(MAIN) 

pdf:
	$(LATEX)
	@echo ""
	@echo "THE LATEX FILE $(MAIN) GOT GENERATED!"
	@echo ""

allfiles:
	$(GP) $(FIG)
	$(LATEX)
	$(BIB)
	$(LATEX)
	$(LATEX)

## clean ALL temp files 
clean:
	rm -rf	*.blg \
		*.bib \
   		*.aux \
   		*.nav \
   		*.snm \
		*.bcf \
		*.log \
		*.aux \
		*.xml \
		*.bbl \
		*.out \
		*.toc \
		*.gz  \
		eps-converted-to.pdf


.PHONY: figs print pdf clean

#EOF
